package com.coleman.drop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class MainMenuScreen implements Screen {

	final Drop game;

	OrthographicCamera camera;
	
	private String title1;
	private String title2;
	private float title1_x = 0;
	private float title1_y = 0;
	private float title2_x = 0;
	private float title2_y = 0;

	public MainMenuScreen(final Drop gam) {
		game = gam;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();
		game.batch.setProjectionMatrix(camera.combined);

		title1 = "Welcome to Brick Buster";
		title1_x = (800 - game.font.getBounds(title1).width)/2;
		title1_y = 480/2 + game.font.getBounds(title1).height + 5;
		
		title2 = "Tap anywhere to begin";
		title2_x = (800 - game.font.getBounds(title2).width)/2;
		title2_y = title1_y - game.font.getBounds(title2).height - 5;
		
		game.batch.begin();
		game.font.draw(game.batch, title1, title1_x, title1_y);
		game.font.draw(game.batch, title2, title2_x, title2_y);
		game.batch.end();

		if (Gdx.input.isTouched()) {
			game.setScreen(new GameScreen(game));
			dispose();
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}
}