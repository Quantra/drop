package com.coleman.drop;

import java.io.File;
import java.util.Iterator;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class GameScreen implements Screen {
	final Drop game;

	private Texture ballImage;
	private Texture batImage;
	private Texture brickImage;

	private Sound dropSound;
	private Music rainMusic;

	private OrthographicCamera camera;

	private Rectangle bat;
//	private Array<Rectangle> bricks;
	private Array<Brick> bricks;
	private Array<Rectangle> hit_bricks;
	private Ball ball;

	private float brick_width;
	private float brick_height;
	private Rectangle brick_intersection;
	private float batHitOffset;
	private Integer bricks_hit;
	private double highest_brick;
	private double leftest_brick;

	private float ball_x_speed_max;

    private Array<Texture> brick_images;

    private Texture bg;

	private class Ball extends Rectangle {
		private double x_speed = 0;
		private double y_speed = 0;

		private double x_dir() {
			return this.x_speed / Math.abs(this.x_speed);
		}

		private double y_dir() {
			return this.y_speed / Math.abs(this.y_speed);
		}
	}

    private class Brick extends Rectangle {
        private Integer type = 1;
        private Texture image;
    }

    private void loadLevel() {
        try {
            FileHandle brickxml = Gdx.files.internal("levels/2/Level_bricks.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(brickxml.file());

            doc.getDocumentElement().normalize();

            NodeList brick_list = doc.getElementsByTagName("sprite");

            for (int j=0; j < brick_list.getLength(); j++) {
                Node brick_node = brick_list.item(j);
                Element brick_element = (Element) brick_node;
                Brick new_brick = new Brick();
                new_brick.x = Float.parseFloat(brick_element.getAttribute("x"));
                new_brick.y = 480 - Float.parseFloat(brick_element.getAttribute("y"));
                new_brick.type = Integer.parseInt(brick_element.getAttribute("name")) - 1;
                new_brick.image = brick_images.get(new_brick.type);
                new_brick.width = new_brick.image.getWidth();
                new_brick.height = new_brick.image.getHeight();
                new_brick.y -= new_brick.height;
                System.out.println("NEW BRICK: x=" + new_brick.x + " y=" + new_brick.y + " type=" + new_brick.type + " width=" + new_brick.width + " height=" + new_brick.height);
                bricks.add(new_brick);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//	private void spawnBricks() {
//		brick_height = 24;
////		brick_width = 131;
//		brick_width = 65;
//		bricks.clear();
//		for (int j=1; j<11; j++) {
//			float this_y = 480 - (j * (brick_height + 2)) - 30;
//
//			for (int k=1; k<14; k++ ) {
//				Brick new_brick = new Brick();
//				new_brick.height = brick_height;
//				new_brick.width = brick_width;
//				new_brick.y = this_y;
//
//				// Stagger bricks
////				if (k%2 == 0) {
////					new_brick.y += 15;
////				}
//
//				new_brick.x = (k - 1) * (new_brick.width + 2);
//				if ((j<3 || k<3) || (j>8 || k>11)) {
//					bricks.add(new_brick);
//				}
//			}
//		}
//
//	}

	public GameScreen(final Drop gam) {
		this.game = gam;

		// load the images
		ballImage = new Texture(Gdx.files.internal("ball.png"));
		batImage = new Texture(Gdx.files.internal("bat2.png"));
		brickImage = new Texture(Gdx.files.internal("brick2.png"));

       brick_images = new Array<Texture>();
        // New brick images
        for (int j=1; j<5; j++) {
            String this_path = "bricks/" + j + ".png";
            brick_images.add(new Texture(Gdx.files.internal(this_path)));
        }

        bricks = new Array<Brick>();
        loadLevel();


		// load sound effects and the music
		dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
		rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));

		// start the playback of the background music immediately and loop it
		rainMusic.setLooping(true);
		rainMusic.play();

		// set the camera
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);


		// init bricks hit
		bricks_hit = 0;

		// set max ball x speed
		ball_x_speed_max = 300;

		// make the bat
		bat = new Rectangle();
		bat.x = 800 / 2 - 192 / 2;
		bat.y = 0;
		bat.width = 192;
		bat.height = 32;

		// init the ball
		ball = new Ball();
		ball.width = ball.height = 16;
		ball.x = bat.x + (bat.width - ball.width)/2;
		ball.y = bat.y + bat.height;


		// spawn the bricks
//		bricks = new Array<Brick>();
//		spawnBricks();

		// prep hit bricks
		hit_bricks = new Array<Rectangle>();
	}

	@Override
	public void render(float delta) {
		// Clear the screen and set the bg colour
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Camera gets and update
		camera.update();

		// Draw sprites
		game.batch.setProjectionMatrix(camera.combined);
		game.batch.begin();
		game.batch.draw(batImage, bat.x, bat.y);
		game.batch.draw(ballImage,  ball.x,  ball.y);
		for (Brick mybrick: bricks) {
			game.batch.draw(mybrick.image,  mybrick.x,  mybrick.y);
		}
		game.batch.end();

		// Move bat with touch/mouse
		if(Gdx.input.isTouched()) {
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);
			bat.x = touchPos.x - 128 / 2;
		}

		// Move bat with cursor keys
		if(Gdx.input.isKeyPressed(Keys.LEFT)) bat.x -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) bat.x += 200 * Gdx.graphics.getDeltaTime();

		//Prevent bat going out of bounds
		if(bat.x < 0) bat.x = 0;
		if(bat.x > 800 - bat.width) bat.x = 800 - bat.width;

		// if ball has no speed shoot it up or stick to bat with keys
		if(ball.x_speed == 0 && ball.y_speed == 0) {
			ball.x = bat.x + (bat.width - ball.width)/2;
			
			//Testing
			ball.y=250;

			if(Gdx.input.isTouched() || Gdx.input.isKeyPressed(Keys.SPACE)) {
				ball.y_speed=200;
				ball.x_speed=10;
			}
		}

		//Shift + R to clear and respawn bricks
		if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) && Gdx.input.isKeyPressed(Keys.R)) {
			bricks.clear();
			loadLevel();
		}

		//Make ball bounce off bounds
		if (ball.x <= 0) {
			ball.x = 0;
			ball.x_speed = -ball.x_speed;
		} else if (ball.x >= 800 - ball.width)  {
			ball.x = 800 - ball.width;
			ball.x_speed = -ball.x_speed;
		}
		if ((ball.y <= bat.height) && (!ball.overlaps(bat))) {
			ball.y = bat.height;
			ball.y_speed = -ball.y_speed;
		} else if (ball.y >= 480 - ball.height - 30) {
			ball.y = 480 - ball.height - 30;
			ball.y_speed = -ball.y_speed;
		}

		//Ball hits bat
		if(ball.overlaps(bat)) {
			dropSound.play();
			ball.y = bat.y + bat.height + 1;
			ball.y_speed = -ball.y_speed;

			// Center of bat vs center of ball measured from center of bat
			batHitOffset = (bat.x + bat.width/2) - (ball.x + ball.width/2);

			//Flip direction ball is travelling if near side of bat is hit
			ball.x_speed = -ball.x_speed*(batHitOffset/Math.abs(batHitOffset)*(ball.x_speed/Math.abs(ball.x_speed)));

			// Better bat hit logic to angle the ball more upwards the closer to center the ball hits bat
			double angleFactor = (4 * Math.pow((Math.abs(batHitOffset) / (bat.width / 2)), 2));
			// speed factor speeds up slow balls
			double speedFactor = ball_x_speed_max/ball.x_speed + 30 * (ball.x_speed/Math.abs(ball.x_speed));
			ball.x_speed = ball.x_speed * angleFactor + speedFactor;
		}

		//Max ball x speed
		if (ball.x_speed > 350) { ball.x_speed = 350; } else if (ball.x_speed < - 350) { ball.x_speed = - 350; }
		//		System.out.println(ball.x_speed);

		//Ball hits a brick
		Iterator<Brick> iter = bricks.iterator();
		hit_bricks.clear();
		while(iter.hasNext()) {
			Brick brick = iter.next();
			if (ball.overlaps(brick)) {
				hit_bricks.add(brick);
				dropSound.play();
				iter.remove();
				bricks_hit++;
//				System.out.println(hit_bricks.size);
			}
		}


		if (hit_bricks.size >= 3) {
			// Triple corner hit rev both
			ball.x_speed = -ball.x_speed;
			ball.y_speed = -ball.y_speed;
		} else if (hit_bricks.size >=2 ) {

			// New improved multi hit logic, to take into account non grid layout of bricks
			Rectangle brick_intersection_1 = new Rectangle();
			Intersector.intersectRectangles(ball, hit_bricks.get(0), brick_intersection_1);
			Rectangle brick_intersection_2 = new Rectangle();
			Intersector.intersectRectangles(ball, hit_bricks.get(1), brick_intersection_2);

			// When brick array building logic is improved bricks must be added to the array left to right, top to bottom
			// ie more leftmost bricks appear before righter ones and higher bricks appear before lower ones

			// TODO Test top and bottom conditions thoroughly.
			if (brick_intersection_1.y > ball.y && brick_intersection_2.y > ball.y) {
				//  Both bottom edges hit
//				System.out.println("bottoms");
				highest_brick = (hit_bricks.get(1).y - hit_bricks.get(0).y) / (Math.abs(hit_bricks.get(1).y - hit_bricks.get(0).y));
				if (hit_bricks.get(1).y == hit_bricks.get(0).y) {
					highest_brick = ball.x_dir();
				}
				//				System.out.println(highest_brick)
				ball.x_speed = ball.x_speed * highest_brick * ball.x_dir() * ball.y_dir();
				ball.y_speed = -ball.y_speed;
				
			} else if (brick_intersection_1.y + brick_intersection_1.height < ball.y + ball.height
					&& brick_intersection_2.y + brick_intersection_2.height < ball.y + ball.height) {
				// Both top edges of both bricks hit
//				System.out.println("tops");
					// Should work in all cases?  Think about bricks with different heights?
				//				System.out.println("------------");

				// -1 left brick is higher, +1 right brick is higher
				highest_brick = (hit_bricks.get(1).y+hit_bricks.get(1).height - hit_bricks.get(0).y+hit_bricks.get(0).height)
						/ (Math.abs(hit_bricks.get(1).y+hit_bricks.get(1).height - hit_bricks.get(0).y+hit_bricks.get(0).height));
				if (hit_bricks.get(1).y+hit_bricks.get(1).height == hit_bricks.get(0).y+hit_bricks.get(0).height) {
					highest_brick = -ball.x_dir();
				}
				//				System.out.println(highest_brick)
				ball.x_speed = ball.x_speed * highest_brick * ball.x_dir() * ball.y_dir();
				ball.y_speed = -ball.y_speed;

			} else if (brick_intersection_1.x > ball.x && brick_intersection_2.x > ball.x ) {
				// Both left edges of both bricks hit
//				System.out.println("lefties");
				double leftest_brick = (hit_bricks.get(0).x - hit_bricks.get(1).x) / (Math.abs(hit_bricks.get(0).x - hit_bricks.get(1).x));
//				System.out.println(leftest_brick);
				if (hit_bricks.get(0).x == hit_bricks.get(1).x) {
//					System.out.println(leftest_brick);
					leftest_brick = ball.y_dir();
				}
//				System.out.println("-----------");
//				System.out.println(ball.x_speed);
//				System.out.println(ball.y_speed);
//				System.out.println(leftest_brick);
//				System.out.println(ball.x_dir());
//				System.out.println(ball.y_dir());
//				System.out.println("==============");
				ball.y_speed = ball.y_speed * leftest_brick * ball.y_dir() * ball.x_dir();
				ball.x_speed = -ball.x_speed;
			}
			else if (brick_intersection_1.x + brick_intersection_1.width < ball.x + ball.width
					&& brick_intersection_2.x + brick_intersection_2.width < ball.x + ball.width) {
				// Both right edges of both bricks hit
//				System.out.println("righties");
//				
//				System.out.println("++++++++++");
//				System.out.println(hit_bricks.get(0).x - hit_bricks.get(1).x);
//				System.out.println(Math.abs(hit_bricks.get(0).x - hit_bricks.get(1).x));
//				System.out.println("____________");
				
				// -1 top brick more left, +1 bottom brick more left
				double leftest_brick = (hit_bricks.get(0).x+hit_bricks.get(0).width - hit_bricks.get(1).x+hit_bricks.get(1).width)
						/ (Math.abs(hit_bricks.get(0).x+hit_bricks.get(0).width - hit_bricks.get(1).x+hit_bricks.get(1).width));
//				System.out.println(leftest_brick);
				if (hit_bricks.get(0).x+hit_bricks.get(0).width == hit_bricks.get(1).x+hit_bricks.get(1).width) {
//					System.out.println(leftest_brick);
					leftest_brick = -ball.y_dir();
				}
//				System.out.println("-----------");
//				System.out.println(ball.x_speed);
//				System.out.println(ball.y_speed);
//				System.out.println(leftest_brick);
//				System.out.println(ball.x_dir());
//				System.out.println(ball.y_dir());
//				System.out.println("==============");
				ball.y_speed = ball.y_speed * leftest_brick * ball.y_dir() * ball.x_dir();
				ball.x_speed = -ball.x_speed;
			} else {
//				System.out.println("corner hit");
				// One vert edge and one horz edge hit on each brick, ie corner hit
				ball.x_speed = -ball.x_speed;
				ball.y_speed = -ball.y_speed;
			}

		} else if (hit_bricks.size >=1) {
			// single brick hit
			brick_intersection = new Rectangle();
			Intersector.intersectRectangles(ball, hit_bricks.get(0), brick_intersection);
			if(brick_intersection.y > ball.y) {
				//bottom hit go down
				ball.y_speed = -Math.abs(ball.y_speed);
			}
			if (brick_intersection.y + brick_intersection.height < ball.y + ball.height) {
				//top hit
				ball.y_speed = Math.abs(ball.y_speed);
			}
			if (brick_intersection.x > ball.x) {
				//hit left
				ball.x_speed = -Math.abs(ball.x_speed);
			}
			if (brick_intersection.x + brick_intersection.width < ball.x + ball.width) {
				//hit right
				ball.x_speed = Math.abs(ball.x_speed);
			}
		}

		//Move the ball
		ball.y += ball.y_speed * Gdx.graphics.getDeltaTime();
		ball.x += ball.x_speed * Gdx.graphics.getDeltaTime();

		//No bricks remain, restart
		if (bricks.size <= 0) {
			ball.x_speed = 0;
			ball.y_speed = 0;
			ball.x = bat.x + (bat.width - ball.width)/2;
			ball.y = bat.y + bat.height;
			loadLevel();
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		// start the playback of the background music
		// when the screen is shown
		rainMusic.play();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}


	@Override
	public void dispose() {
		ballImage.dispose();
		batImage.dispose();
		brickImage.dispose();
		dropSound.dispose();
		rainMusic.dispose();
        for (Texture brick_image:brick_images) {
            brick_image.dispose();
        }
	}

}
